#!/bin/sh

cat <<_EOF_
#ifndef _MINIFMT_IFMT_H
#define _MINIFMT_IFMT_H

#include <minifmt.h>
_EOF_

${SED} -f ${srcdir}/build-aux/ifmt-decls.sed

cat <<_EOF_
#endif /* _MINIFMT_IFMT_H */
_EOF_
