#!/bin/sh

cat <<_EOF_
#include <minifmt.h>
#include <stdbool.h>
_EOF_

${SED} -f ${srcdir}/build-aux/ifmt-impls.sed
