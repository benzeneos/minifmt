#!/bin/sed -f

s|^IFMT (\([^,]*\), \([^)]*\))$|#define FUNCTION minifmt_ifmt_\2\
#define TYPE \1\
#include "ifmt.part.h"\
#undef FUNCTION\
#undef TYPE\
|g
