/*
 * Copyright (C) Jakub Kaszycki
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHORS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef TYPE
#error Do not include this file directly!
#endif

#define CONCAT1(x, y) x##y
#define CONCAT(x, y) CONCAT1 (x, y)

#define RADIX_CHAR_TABLE CONCAT (FUNCTION, _RADIX_CHAR_TABLE)
#define RADIX_CHAR_TABLE_UPPER CONCAT (FUNCTION, _RADIX_CHAR_TABLE_UPPER)

static const char RADIX_CHAR_TABLE[] = {
  '0', '1', '2', '3', '4', '5',
  '6', '7', '8', '9', 'a', 'b',
  'c', 'd', 'e', 'f', 'g', 'h',
  'i', 'j', 'k', 'l', 'm', 'n',
  'o', 'p', 'q', 'r', 's', 't',
  'u', 'v', 'w', 'x', 'y', 'z'
};

static const char RADIX_CHAR_TABLE_UPPER[] = {
  '0', '1', '2', '3', '4', '5',
  '6', '7', '8', '9', 'A', 'B',
  'C', 'D', 'E', 'F', 'G', 'H',
  'I', 'J', 'K', 'L', 'M', 'N',
  'O', 'P', 'Q', 'R', 'S', 'T',
  'U', 'V', 'W', 'X', 'Y', 'Z'
};

minifmt_ifmt_return
FUNCTION (const minifmt_ifmt_args * args, TYPE value,
          minifmt_out_stream * strm)
{
  minifmt_ifmt_return ret;

  ret.error = MINIFMT_SUCCESS;
  ret.suberror = 0;
  ret.written = 0;

  if (args->radix < 2 || args->radix > 36)
    {
      ret.error = MINIFMT_ERROR_RADIX;
      return ret;
    }

  size_t digits = 0;
  size_t digits_add = 0;

  bool neg = false;

  if (value < ((TYPE) 0) || args->nonnegspace || args->forcesign)
    {
      digits_add++;
    }
  if (value < ((TYPE) 0))
    {
      value = -value;
      neg = true;
    }

  /* If we could rely on standard <math.h>, this would get replaced by calls to
     log and rint */
  {
    TYPE copy = value;

    do
      {
        digits++;

        copy /= ((TYPE) args->radix);


      }
    while (copy != ((TYPE) 0));
  }

  const char *alt_form_str = "";

  if (args->alt_form)
    {
      switch (args->radix)
        {
        case 2:
          alt_form_str = "0b";
          digits_add += 2;
          break;
        case 8:
          alt_form_str = "0";
          digits_add++;
          break;
        case 16:
          alt_form_str = "0x";
          digits_add += 2;
          break;
        }
    }

  size_t digits_all = digits + digits_add;

  if (args->fmtarg.padding == MINIFMT_PAD_RIGHT)
    {
      if (args->fmtarg.pad_char == '0')
        {
          if (neg)
            {
              ret.error = strm->putc (strm->putc_userdata, '-');
              if (ret.error != MINIFMT_SUCCESS)
                return ret;
              else
                ret.written++;
            }
          else
            {
              if (args->forcesign)
                {
                  ret.error = strm->putc (strm->putc_userdata, '+');
                  if (ret.error != MINIFMT_SUCCESS)
                    return ret;
                  else
                    ret.written++;
                }
              else if (args->nonnegspace)
                {
                  ret.error = strm->putc (strm->putc_userdata, ' ');
                  if (ret.error != MINIFMT_SUCCESS)
                    return ret;
                  else
                    ret.written++;
                }
            }
          while (*alt_form_str)
            {
              ret.error = strm->putc (strm->putc_userdata, *alt_form_str);
              if (ret.error != MINIFMT_SUCCESS)
                return ret;
              else
                ret.written++;
              alt_form_str++;
            }
        }
      for (size_t i = digits_all; i < args->fmtarg.pad_size; i++)
        {
          ret.error = strm->putc (strm->putc_userdata, args->fmtarg.pad_char);
          if (ret.error != MINIFMT_SUCCESS)
            return ret;
          else
            ret.written++;
        }
      if (args->fmtarg.pad_char != '0')
        {
          if (neg)
            {
              ret.error = strm->putc (strm->putc_userdata, '-');
              if (ret.error != MINIFMT_SUCCESS)
                return ret;
              else
                ret.written++;
            }
          else
            {
              if (args->forcesign)
                {
                  ret.error = strm->putc (strm->putc_userdata, '+');
                  if (ret.error != MINIFMT_SUCCESS)
                    return ret;
                  else
                    ret.written++;
                }
              else if (args->nonnegspace)
                {
                  ret.error = strm->putc (strm->putc_userdata, ' ');
                  if (ret.error != MINIFMT_SUCCESS)
                    return ret;
                  else
                    ret.written++;
                }
            }
          while (*alt_form_str)
            {
              ret.error = strm->putc (strm->putc_userdata, *alt_form_str);
              if (ret.error != MINIFMT_SUCCESS)
                return ret;
              else
                ret.written++;
              alt_form_str++;
            }
        }
    }


  for (size_t i = 0; i < digits; i++)
    {
      TYPE copy = value;

      for (size_t j = digits - 1; j > i; j--)
        {
          copy /= ((TYPE) args->radix);
        }

      ret.error = strm->putc (strm->putc_userdata,
                                      (args->upper ? RADIX_CHAR_TABLE_UPPER :
                                       RADIX_CHAR_TABLE)[copy % ((TYPE)
                                                                 args->radix)]);

      if (ret.error != MINIFMT_SUCCESS)
        return ret;
      else
        ret.written++;
    }

  if (args->fmtarg.padding == MINIFMT_PAD_LEFT)
    {
      for (size_t i = digits_all; i < args->fmtarg.pad_size; i++)
        {
          ret.error = strm->putc (strm->putc_userdata, args->fmtarg.pad_char);

          if (ret.error != MINIFMT_SUCCESS)
            return ret;
          else
            ret.written++;
        }
    }

  return ret;
}

#undef RADIX_CHAR_TABLE
#undef RADIX_CHAR_TABLE_UPPER
#undef CONCAT
#undef CONCAT1
