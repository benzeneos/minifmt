/*
 * Copyright (C) Jakub Kaszycki
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHORS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <minifmt.h>

static inline bool
_xisdigit (int ch)
{
  return ch >= '0' && ch <= '9';
}

static inline unsigned int
_xatoi (const char **ptr)
{
  unsigned int val = 0;
  bool digits_started = false;

  while (**ptr)
    {
      if (!digits_started && !_xisdigit (**ptr))
        continue;
      if (digits_started && !_xisdigit (**ptr))
        break;
      if (!digits_started && _xisdigit (**ptr))
        digits_started = true;
      val *= 10;
      val += **ptr - '0';

      (*ptr)++;
    }

  return val;
}

static inline int
_xtoupper (int ch)
{
  return ch + ('a' - 'A');
}

#define GETASFUNC_CASE(type,typemacro,typefield) \
    case typemacro: \
      return (type) val.typefield
#define GETASFUNC(n,t) \
static inline t \
_xgetas##n (minifmt_value val) \
{ \
  switch (val.type) \
    { \
      GETASFUNC_CASE (t, MINIFMT_VALUE_CHAR, ch); \
      GETASFUNC_CASE (t, MINIFMT_VALUE_SHORT, sh); \
      GETASFUNC_CASE (t, MINIFMT_VALUE_INT, i); \
      GETASFUNC_CASE (t, MINIFMT_VALUE_LONG, l); \
      GETASFUNC_CASE (t, MINIFMT_VALUE_LLONG, ll); \
      GETASFUNC_CASE (t, MINIFMT_VALUE_SSIZE, ssize); \
      GETASFUNC_CASE (t, MINIFMT_VALUE_FLOAT, flt); \
      GETASFUNC_CASE (t, MINIFMT_VALUE_DOUBLE, dbl); \
      GETASFUNC_CASE (t, MINIFMT_VALUE_LDOUBLE, ldbl); \
      GETASFUNC_CASE (t, MINIFMT_VALUE_SIZE, size); \
      GETASFUNC_CASE (t, MINIFMT_VALUE_INTMAX, intmax); \
      GETASFUNC_CASE (t, MINIFMT_VALUE_UINTMAX, uintmax); \
      GETASFUNC_CASE (t, MINIFMT_VALUE_PTRDIFF, ptrdiff); \
    case MINIFMT_VALUE_POINTER: \
      return (t) (long) val.pointer; \
    default: \
      return 0; \
    } \
   \
  return MINIFMT_SUCCESS; \
}

GETASFUNC (char, char)
GETASFUNC (short, short)
GETASFUNC (int, int)
GETASFUNC (long, long)
GETASFUNC (llong, long long)
GETASFUNC (size, size_t)
GETASFUNC (ssize, ssize_t)
GETASFUNC (intmax, intmax_t)
GETASFUNC (uintmax, uintmax_t)
GETASFUNC (ptrdiff, ptrdiff_t)
GETASFUNC (float, float)
GETASFUNC (double, double)
GETASFUNC (ldouble, long double)

#undef GETASFUNC_CASE
#define GETASFUNC_CASE(type,typemacro,typefield) \
    case typemacro: \
      return (type) (long) val.typefield

GETASFUNC (pointer, void *)

#undef GETASFUNC
#undef GETASFUNC_CASE

/* TODO: Floats */

minifmt_out_return
minifmt_out (minifmt_out_stream *stream, const char *format,
             minifmt_value_vec args)
{
#define setup_which() \
  if (which == -1) \
    { \
      which = args_index++; \
    } \
  if (which >= ((long) args.count)) \
    { \
      ret.error = MINIFMT_ERROR_FORMAT; \
      ret.suberror = MINIFMT_ERROR_FORMAT_ARGEXCEED; \
      goto mainloop_end; \
    }
#define set_error(e, s) \
  do \
    { \
      ret.error = e; \
      ret.suberror = s; \
      goto mainloop_end; \
    } \
  while (0)
  minifmt_fmt_args fmt_args;
  minifmt_ifmt_args ifmt_args;
  /* minifmt_ffmt_args ffmt_args; */
  minifmt_out_return ret;
  size_t args_index = 0;
  char size = '\0';

  fmt_args.pad_char = ' ';
  fmt_args.padding = MINIFMT_PAD_RIGHT;
  fmt_args.pad_size = 0;
  fmt_args.prec = ((size_t) -1); // Special, reserved value

  ifmt_args.alt_form = false;
  ifmt_args.radix = 10;
  ifmt_args.nonnegspace = false;
  ifmt_args.upper = false;
  ifmt_args.forcesign = false;

  ret.written = 0;
  ret.exptowrite = 0;
  ret.error = MINIFMT_SUCCESS;

  while (*format)
    {
      switch (*format)
        {
        case '%':
          format++;
          long which = -1;
          if (_xisdigit (*format))
            {
              bool matches = true;
              const char *format_backup = format;
              while (*format)
                {
                  if (*format == '$')
                    {
                      break;
                    }
                  else if (!_xisdigit (*format))
                    {
                      if (*format == 0)
                        {
                          set_error (MINIFMT_ERROR_FORMAT,
                                     MINIFMT_ERROR_FORMAT_EOF);
                        }
                      matches = false;
                      break;
                    }
                  format++;
                }
              format = format_backup;
              if (matches)
                {
                  which = _xatoi (&format) - 1;
                  if (*format != '$')
                    {
                      set_error (MINIFMT_ERROR_FORMAT, 0);
                    }
                  format++;
                }
            }
          while (1)
            {
              switch (*format)
                {
                case '#':
                  ifmt_args.alt_form = true;
                  /*
                     ffmt_args.alt_form = true;
                   */
                  break;
                case '0':
                  if (fmt_args.pad_char == '0')
                    {
                      // This is a part of field number
                      goto after_loop1;
                    }
                  fmt_args.pad_char = '0';
                  break;
                case '-':
                  fmt_args.padding = MINIFMT_PAD_LEFT;
                  fmt_args.pad_char = ' ';
                  break;
                case ' ':
                  /* If - was not given, because printf doesn't want us to override
                     it. Also overriden by + */
                  if (fmt_args.padding != MINIFMT_PAD_LEFT && !ifmt_args.forcesign)
                    {
                      ifmt_args.nonnegspace = true;
                      /* 
                         ffmt_args.nonnegspace = true;
                       */
                    }
                  break;
                case '+':
                  ifmt_args.forcesign = true;
                  ifmt_args.nonnegspace = false;
                  /*
                     ffmt_args.forcesign = true;
                     ffmt_args.nonnegspace = false;
                   */
                  break;
                case '\0':
                  set_error (MINIFMT_ERROR_FORMAT, MINIFMT_ERROR_FORMAT_EOF);
                default:
                  goto after_loop1;
                }
              format++;
            }
after_loop1:
          if (_xisdigit (*format))
            {
              fmt_args.pad_size = _xatoi (&format);
            }
          else if (*format == '*')
            {
              format++;
              if (_xisdigit (*format))
                {
                  unsigned int which_pad = _xatoi (&format);
                  if (which_pad >= args.count)
                    {
                      set_error (MINIFMT_ERROR_FORMAT,
                                 MINIFMT_ERROR_FORMAT_ARGEXCEED);
                    }
                  minifmt_value val = args.values[which_pad];
                  fmt_args.pad_size = _xgetassize (val);
                }
              else
                {
                  if (args_index >= args.count)
                    {
                      set_error (MINIFMT_ERROR_FORMAT,
                                 MINIFMT_ERROR_FORMAT_ARGEXCEED);
                    }
                  minifmt_value val = args.values[args_index++];
                  fmt_args.pad_size = _xgetassize (val);
                }
            }
          if (*format == 0)
            {
              set_error (MINIFMT_ERROR_FORMAT, MINIFMT_ERROR_FORMAT_EOF);
            }
          if (*format == '.')
            {
              format++;
              if (_xisdigit (*format))
                {
                  fmt_args.prec = _xatoi (&format);
                }
              else if (*format == '*')
                {
                  format++;
                  if (_xisdigit (*format))
                    {
                      unsigned int which_prec = _xatoi (&format);
                      if (which_prec >= args.count)
                        {
                          set_error (MINIFMT_ERROR_FORMAT,
                                     MINIFMT_ERROR_FORMAT_ARGEXCEED);
                        }
                      minifmt_value val = args.values[which_prec];
                      fmt_args.prec = _xgetassize (val);
                    }
                  else
                    {
                      if (args_index >= args.count)
                        {
                          ret.error = MINIFMT_ERROR_FORMAT;
                          ret.suberror = MINIFMT_ERROR_FORMAT_ARGEXCEED;
                          goto mainloop_end;
                        }
                      minifmt_value val = args.values[args_index++];
                      fmt_args.prec = _xgetassize (val);
                    }
                }
              else
                {
                  fmt_args.prec = 0;
                }
            }
          while (1)
            {
              switch (*format)
                {
                case 'h':
                case 'l':
                  if (size == '\0')
                    {
                      size = *format;
                    }
                  else if (size == *format)
                    {
                      size = _xtoupper (*format);
                    }
                  else
                    {
                      set_error (MINIFMT_ERROR_FORMAT, 0);
                    }
                  break;
                case 'H':
                case 'L':
                case 'j':
                case 'z':
                case 't':
                  if (size != '\0')
                    {
                      set_error (MINIFMT_ERROR_FORMAT, 0);
                    }
                  size = *format;
                  break;
                case '\0':
                  set_error (MINIFMT_ERROR_FORMAT, MINIFMT_ERROR_FORMAT_EOF);
                default:
                  goto after_loop2;
                }
              format++;
            }
after_loop2:
          ifmt_args.fmtarg = fmt_args;
          switch (*format)
            {
            case 'd':
            case 'i':
              {
                setup_which ();

                ifmt_args.radix = 10;

                minifmt_ifmt_return iret;

                minifmt_value val = args.values[which];

                switch (size)
                  {
                  case 'H':
                    iret = minifmt_ifmt_schar (&ifmt_args, _xgetaschar (val), stream);
                    break;
                  case 'h':
                    iret = minifmt_ifmt_sshort (&ifmt_args, _xgetasshort (val), stream);
                    break;
                  case '\0':
                    iret = minifmt_ifmt_sint (&ifmt_args, _xgetasint (val), stream);
                    break;
                  case 'l':
                    iret = minifmt_ifmt_slong (&ifmt_args, _xgetaslong (val), stream);
                    break;
                  case 'L':
                    iret = minifmt_ifmt_sllong (&ifmt_args, _xgetasllong (val), stream);
                    break;
                  case 'j':
                    iret = minifmt_ifmt_ssize (&ifmt_args, _xgetasssize (val), stream);
                    break;
                  case 'z':
                    iret = minifmt_ifmt_intmax (&ifmt_args, _xgetasintmax (val), stream);
                    break;
                  case 't':
                    iret = minifmt_ifmt_ptrdiff (&ifmt_args, _xgetasptrdiff (val), stream);
                    break;
                  }

                ret.exptowrite += iret.written;

                if (iret.error != MINIFMT_SUCCESS)
                  {
                    set_error (iret.error, iret.suberror);
                  }
                else
                  {
                    ret.written += iret.written;
                  }

                break;
              }
            case 'u':
              {
                setup_which ();

                ifmt_args.radix = 10;

                minifmt_ifmt_return iret;

                minifmt_value val = args.values[which];

                switch (size)
                  {
                  case 'H':
                    iret = minifmt_ifmt_uchar (&ifmt_args, _xgetaschar (val), stream);
                    break;
                  case 'h':
                    iret = minifmt_ifmt_ushort (&ifmt_args, _xgetasshort (val), stream);
                    break;
                  case '\0':
                    iret = minifmt_ifmt_uint (&ifmt_args, _xgetasint (val), stream);
                    break;
                  case 'l':
                    iret = minifmt_ifmt_ulong (&ifmt_args, _xgetaslong (val), stream);
                    break;
                  case 'L':
                    iret = minifmt_ifmt_ullong (&ifmt_args, _xgetasllong (val), stream);
                    break;
                  case 'j':
                    iret = minifmt_ifmt_size (&ifmt_args, _xgetassize (val), stream);
                    break;
                  case 'z':
                    iret = minifmt_ifmt_uintmax (&ifmt_args, _xgetasuintmax (val), stream);
                    break;
                  case 't':
                    /* We don't have any unsigned ptrdiff_t variant, but we can
                       use the trick: long is always the same size as a pointer */
                    iret = minifmt_ifmt_ulong (&ifmt_args, _xgetaslong (val), stream);
                    break;
                  }

                ret.exptowrite += iret.written;

                if (iret.error != MINIFMT_SUCCESS)
                  {
                    set_error (iret.error, iret.suberror);
                  }
                else
                  {
                    ret.written += iret.written;
                  }

                break;
              }
            case 'x':
              {
                setup_which ();

                ifmt_args.radix = 16;

                minifmt_ifmt_return iret;

                minifmt_value val = args.values[which];

                switch (size)
                  {
                  case 'H':
                    iret = minifmt_ifmt_uchar (&ifmt_args, _xgetaschar (val), stream);
                    break;
                  case 'h':
                    iret = minifmt_ifmt_ushort (&ifmt_args, _xgetasshort (val), stream);
                    break;
                  case '\0':
                    iret = minifmt_ifmt_uint (&ifmt_args, _xgetasint (val), stream);
                    break;
                  case 'l':
                    iret = minifmt_ifmt_ulong (&ifmt_args, _xgetaslong (val), stream);
                    break;
                  case 'L':
                    iret = minifmt_ifmt_ullong (&ifmt_args, _xgetasllong (val), stream);
                    break;
                  case 'j':
                    iret = minifmt_ifmt_size (&ifmt_args, _xgetassize (val), stream);
                    break;
                  case 'z':
                    iret = minifmt_ifmt_uintmax (&ifmt_args, _xgetasuintmax (val), stream);
                    break;
                  case 't':
                    /* We don't have any unsigned ptrdiff_t variant, but we can
                       use the trick: long is always the same size as a pointer */
                    iret = minifmt_ifmt_ulong (&ifmt_args, _xgetaslong (val), stream);
                    break;
                  }

                ret.exptowrite += iret.written;

                if (iret.error != MINIFMT_SUCCESS)
                  {
                    set_error (iret.error, iret.suberror);
                  }
                else
                  {
                    ret.written += iret.written;
                  }

                break;
              }
            case 's':
              {
                setup_which ();

                minifmt_value val = args.values[which];
                const char *str = (const char *) _xgetaspointer (val);
                if (fmt_args.prec == ((size_t) -1))
                  {
                    while (*str)
                      {
                        minifmt_error err = stream->putc
                          (stream->putc_userdata, *str);
                        ret.exptowrite++;
                        if (err != MINIFMT_SUCCESS)
                          {
                            set_error (err, 0);
                          }
                        else
                          {
                            ret.written++;
                          }
                        str++;
                      }
                  }
                else
                  {
                    for (size_t i = 0; i < fmt_args.prec; i++)
                      {
                        minifmt_error err = stream->putc
                          (stream->putc_userdata, str[i]);
                        ret.exptowrite++;
                        if (err != MINIFMT_SUCCESS)
                          {
                            set_error (err, 0);
                          }
                        else
                          {
                            ret.written++;
                          }
                      }
                  }
                break;
              }
            case 'c':
              {
                setup_which ();

                minifmt_value val = args.values[which];

                minifmt_error err = stream->putc (stream->putc_userdata,
                                                  _xgetaschar (val));
                ret.exptowrite++;
                if (err != MINIFMT_SUCCESS)
                  {
                    set_error (err, 0);
                  }
                else
                  {
                    ret.written++;
                  }
                break;
              }
            default:
              {
                set_error (MINIFMT_ERROR_FORMAT, 0);
              }
            }
          break;
        case '\0':
          goto mainloop_end;
        default:;
          minifmt_error err = stream->putc (stream->putc_userdata, *format);
          ret.exptowrite++;
          if (err != MINIFMT_SUCCESS)
            {
              set_error (err, 0);
            }
          else
            {
              ret.written++;
            }
          break;
        }

      format ++;
    }

mainloop_end:
  return ret;
}
