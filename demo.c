/*
 * Copyright (C) Jakub Kaszycki
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHORS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <minifmt.h>
#include <stdio.h>

static minifmt_value ARGS[] = {
  {
    .type = MINIFMT_VALUE_INT,
    .i = 2137
  },
  {
    .type = MINIFMT_VALUE_POINTER,
    .pointer = "Hello, World! If you see this message, there is a trouble\n"
  },
  {
    .type = MINIFMT_VALUE_CHAR,
    .ch = '!'
  }
};

static minifmt_error
xputc (void *udata, int ch)
{
  return fputc (ch, (FILE *) udata) == EOF ? MINIFMT_ERROR_IO :
    MINIFMT_SUCCESS;
}

static void
out (minifmt_out_stream *ost, const char *fmt, minifmt_value_vec v)
{
  minifmt_out_return ret = minifmt_out (ost, fmt, v);
  if (ret.error != MINIFMT_SUCCESS)
    {
      printf ("ERR %s\nSUB %s\n", minifmt_errstr (ret.error),
              minifmt_suberrstr (ret.error, ret.suberror));
    }
  printf ("WRITTEN %u\nEXP     %u\n", ret.written, ret.exptowrite);
}

int
main (void)
{
  minifmt_out_stream ost;
  
  ost.putc = xputc;
  ost.putc_userdata = (void *) stdout;

  minifmt_value_vec v = { 3, ARGS };

  out (&ost, "%+0#80x\n", v);
  out (&ost, "%1$#80ju\n", v);
  out (&ost, "%2$.12s%3$c\n", v);

  return 0;
}
