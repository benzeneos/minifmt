// vim: ft=cpp.doxygen
/*
 * Copyright (C) Jakub Kaszycki
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHORS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef _MINIFMT_H
#define _MINIFMT_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

typedef long ssize_t;

/**
 * Error code for calls.
 * 
 * @since 0.1.0
 */
typedef enum
{
  /**
   * Success - no error.
   * 
   * @since 0.1.0
   */
  MINIFMT_SUCCESS = 0,

  /**
   * I/O related error.
   * 
   * @since 0.1.0
   */
  MINIFMT_ERROR_IO,

  /**
   * Format string was invalid.
   * 
   * @since 0.1.0
   */
  MINIFMT_ERROR_FORMAT,

  /**
   * Radix is zero, one, or too high.
   * 
   * @since 0.1.0
   */
  MINIFMT_ERROR_RADIX,

  /* APPEND NEW ENTRIES BEFORE THIS COMMENT */

  /**
   * An unknown error has happened to MiniFmt.
   * 
   * @since 0.1.0
   */
  MINIFMT_ERROR_UNKNOWN = -1,
} minifmt_error;

typedef enum
{
  /**
   * End of format.
   * 
   * @since 0.1.0
   * @see MINIFMT_ERROR_FORMAT
   */
  MINIFMT_ERROR_FORMAT_EOF = 1,

  /**
   * End of arguments while looking for next one <b>OR</b> too far direct
   * argument index reference.
   * 
   * @since 0.1.0
   * @see MINIFMT_ERROR_FORMAT
   */
  MINIFMT_ERROR_FORMAT_ARGEXCEED,
} minifmt_suberror;

/**
 * Type of a value.
 * 
 * @see minifmt_value
 * @since 0.1.0
 */
typedef enum
{
  /**
   * The value is of type <code>char</code>.
   * 
   * @since 0.1.0
   */
  MINIFMT_VALUE_CHAR,

  /**
   * The value is of type <code>short</code>.
   * 
   * @since 0.1.0
   */
  MINIFMT_VALUE_SHORT,

  /**
   * The value is of type <code>int</code>.
   * 
   * @since 0.1.0
   */
  MINIFMT_VALUE_INT,

  /**
   * The value is of type <code>long</code>.
   * 
   * @since 0.1.0
   */
  MINIFMT_VALUE_LONG,

  /**
   * The value is of type <code>long long</code>.
   * 
   * @since 0.1.0
   */
  MINIFMT_VALUE_LLONG,

  /**
   * The value is a pointer.
   * 
   * @since 0.1.0
   */
  MINIFMT_VALUE_POINTER,

  /**
   * The value is a single precision floating-point number.
   * 
   * @since 0.1.0
   */
  MINIFMT_VALUE_FLOAT,

  /**
   * The value is a double precision floating-point number.
   * 
   * @since 0.1.0
   */
  MINIFMT_VALUE_DOUBLE,

  /**
   * The value is a quadruple (but this differs amongst platforms) precision
   * floating-point number.
   * 
   * @since 0.1.0
   */
  MINIFMT_VALUE_LDOUBLE,

  MINIFMT_VALUE_SSIZE,
  MINIFMT_VALUE_SIZE,
  MINIFMT_VALUE_INTMAX,
  MINIFMT_VALUE_UINTMAX,
  MINIFMT_VALUE_PTRDIFF,
} minifmt_value_type;

/**
 * A value which can be passed as an argument to a format.
 * 
 * @since 0.1.0
 */
typedef struct
{
  /**
   * Type of this value.
   * 
   * @since 0.1.0
   */
  minifmt_value_type type;

  /**
   * This value represented as appropriate type.
   * 
   * @since 0.1.0
   */
  union
  {
    /**
     * This value, represented as <code>char</code>.
     * 
     * @since 0.1.0
     * @see MINIFMT_VALUE_CHAR
     */
    char ch;

    /**
     * This value, represented as <code>short</code>.
     * 
     * @since 0.1.0
     * @see MINIFMT_VALUE_SHORT
     */
    short sh;

    /**
     * This value, represented as <code>int</code>.
     * 
     * @since 0.1.0
     * @see MINIFMT_VALUE_INT
     */
    int i;

    /**
     * This value, represented as <code>long</code>.
     * 
     * @since 0.1.0
     * @see MINIFMT_VALUE_LONG
     */
    long l;

    /**
     * This value, represented as <code>long long</code>.
     */
    long long ll;

    /**
     * This value, represented as <code>void *</code>.
     * 
     * @since 0.1.0
     * @see MINIFMT_VALUE_POINTER
     */
    void *pointer;

    /**
     * This value, represented as <code>float</code>.
     * 
     * @since 0.1.0
     * @see MINIFMT_VALUE_FLOAT
     */
    float flt;

    /**
     * This value, represented as <code>double</code>.
     * 
     * @since 0.1.0
     * @see MINIFMT_VALUE_DOUBLE
     */
    double dbl;

    /**
     * This value, represented as <code>long double</code>.
     * 
     * @since 0.1.0
     * @see MINIFMT_VALUE_LDOUBLE
     */
    long double ldbl;

    ssize_t ssize;
    size_t size;
    intmax_t intmax;
    uintmax_t uintmax;
    ptrdiff_t ptrdiff;
  };
} minifmt_value;

/**
 * Vector of values.
 * 
 * @since 0.1.0
 */
typedef struct
{
  /**
   * Number of values.
   * 
   * @since 0.1.0
   */
  size_t count;

  /**
   * The actual values.
   * 
   * @since 0.1.0
   */
  minifmt_value *values;
} minifmt_value_vec;

/**
 * Callback function which writes one character to the actual stream.
 * 
 * @param userdata data which can be passed by user
 * @param ch character to be written
 * @return error code or zero for success
 */
typedef minifmt_error (*minifmt_out_putchar_callback) (void *userdata, int ch);


/**
 * Output stream.
 * 
 * @since 0.1.0
 */
typedef struct
{
  /* TODO: Buffers, etc */

  /**
   * Write-character callback.
   * 
   * @since 0.1.0
   */
  minifmt_out_putchar_callback putc;

  /**
   * Argument for write-character callback.
   * 
   * @since 0.1.0
   * @see putc
   */
  void *putc_userdata;
} minifmt_out_stream;

/**
 * Return status of output-format function.
 * 
 * @since 0.1.0
 */
typedef struct
{
  /**
   * The number of characters written actually.
   * 
   * @since 0.1.0
   */
  size_t written;

  /**
   * The number of characters expected to be written.
   * 
   * @since 0.1.0
   */
  size_t exptowrite;

  /**
   * Error code.
   * 
   * @since 0.1.0
   */
  minifmt_error error;

  /**
   * Error sub-code.
   * 
   * @since 0.1.0
   */
  minifmt_suberror suberror;
} minifmt_out_return;

/**
 * Type of padding of formatted string.
 * 
 * @since 0.1.0
 */
typedef enum
{
  /**
   * Default. Does not do any padding at all.
   * 
   * @since 0.1.0
   */
  MINIFMT_PAD_NONE = 0,

  /**
   * Pads the string left.
   * 
   * @since 0.1.0
   */
  MINIFMT_PAD_LEFT,

  /**
   * Pads the string right.
   * 
   * @since 0.1.0
   */
  MINIFMT_PAD_RIGHT
} minifmt_pad_type;

/**
 * Format arguments.
 * 
 * @since 0.1.0
 */
typedef struct
{
  minifmt_pad_type padding;
  char pad_char;
  size_t pad_size;
  size_t prec;
} minifmt_fmt_args;

typedef struct
{
  /**
   * Arguments common for all formats.
   *
   * @since 0.1.0
   */
  minifmt_fmt_args fmtarg;

  /**
   * Radix to print the integer.
   * 
   * @since 0.1.0
   */
  unsigned int radix;

  bool nonnegspace;
  bool forcesign;
  bool alt_form;
  bool upper;
} minifmt_ifmt_args;

typedef struct
{
  minifmt_error error;
  minifmt_suberror suberror;
  size_t written;
} minifmt_ifmt_return;

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * Formats a string.
 * 
 * @since 0.1.0
 */
minifmt_out_return minifmt_out (minifmt_out_stream *, const char *format,
                                minifmt_value_vec);

const char *minifmt_errstr (minifmt_error);
const char *minifmt_suberrstr (minifmt_error, minifmt_suberror);

#include <minifmt/ifmt.h>

#ifdef __cplusplus
}
#endif

#endif /* _MINIFMT_H */
