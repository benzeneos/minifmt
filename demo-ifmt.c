/*
 * Copyright (C) Jakub Kaszycki
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHORS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <minifmt.h>

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

static minifmt_error
xputc (void *udata, int ch)
{
  return fputc (ch, (FILE *) udata) == EOF ? MINIFMT_ERROR_IO :
    MINIFMT_SUCCESS;
}

int
main (void)
{
  minifmt_ifmt_args arg;
  minifmt_out_stream ost;
  
  ost.putc = xputc;
  ost.putc_userdata = (void *) stdout;

  printf ("Radix: ");
  scanf ("%u", &arg.radix);
  printf ("Alt form? ");
  scanf ("%hhu", &arg.alt_form);
  printf ("Force sign? ");
  scanf ("%hhu", &arg.forcesign);
  printf ("Upper case? ");
  scanf ("%hhu", &arg.upper);
  printf ("Space before non-negative? ");
  scanf ("%hhu", &arg.nonnegspace);
  printf ("Padding type: ");
  scanf ("%u", &arg.fmtarg.padding);
  if (arg.fmtarg.padding)
    {
      printf ("Pad char: ");
        {
          int ch;
          while (isspace (ch = getchar ())) ;
          arg.fmtarg.pad_char = (char) ch;
        }
      printf ("Pad size: ");
      scanf ("%zu", &arg.fmtarg.pad_size);
    }
  printf ("Precision: ");
  scanf ("%zu", &arg.fmtarg.prec);
  printf ("Signed: ");
  bool is_signed;
  scanf ("%hhu", &is_signed);
  printf ("Number: ");
  minifmt_ifmt_return ret;
  if (is_signed)
    {
      intmax_t num;
      scanf ("%jd", &num);
      ret = minifmt_ifmt_intmax (&arg, num, &ost);
    }
  else
    {
      uintmax_t num;
      scanf ("%ju", &num);
      ret = minifmt_ifmt_uintmax (&arg, num, &ost);
    }

  printf ("Error:     %s\n", minifmt_errstr (ret.error));
  printf ("Sub-error: %s\n", minifmt_suberrstr (ret.error, ret.suberror));
  printf ("Written:   %zu\n", ret.written);
  return ret.error == MINIFMT_SUCCESS ? EXIT_SUCCESS : EXIT_FAILURE;
}
